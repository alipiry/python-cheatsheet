#!/usr/bin/env python3

# print string
first_name = 'Ali'
last_name = 'Piry'
full_name = first_name + ' ' + last_name

print("Hello, " + full_name)

# user input - string
name = input("What's your name? ")
print("Hello, " + name + "!")

# user input - number
age = input("How old are you? ")
age = int(age)

# list
languages = ['js', 'python', 'go', 'c++']

# get the first item of list
first_lang = languages[0]

# get the last item of list
last_lang = languages[-1]

# looping through a list
for lang in languages:
    print(lang)

# adding items to a list
names = []
names.append('ali')
names.append('mamad')

# making numerical list
squeres = []
for x in range(1, 11):
    squeres.append(x**2)

# list comprehensions
new_squares = [x**2 for x in range(1, 11)]

# slicing a list
my_list = ['ali', 'mamad', 'amir']
first_two_of_my_list = my_list[:2]

# copying a list
copy_of_my_list = my_list[:]

# making tuple: a list that cannot change
dimensions = (1920, 1080)

# dict
me = {'name': 'alipiry', 'age': 20}
print("My name is " + me['name'] + " , and I'm " + str(me['age']))

# add new key-value pair to dict
me['height'] = 185

# looping through all key-value pairs in dict
numbers = {'one': 1, 'two': 2}
for name, number in numbers.items():
    print(name + ' equals ' + str(number))'}

# looping through all keys
for name in numbers.keys():
    print(name + ' is a number')

# looping through all values
for number in fav_numbers.values():
    print(str(number) + ' is a number')
